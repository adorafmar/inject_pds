const fs = require("fs");
const request = require("request-promise");
const { Parser } = require('json2csv');
const AWS = require('aws-sdk');
const fields = ['time', 'building', 'utility', 'value', 'quality'];
const opts = { fields };
const parser = new Parser(opts);
const ONE_SECOND = 1000;
const ONE_MINUTE = 60*ONE_SECOND;
const TWO_MINUTES = 2*60*ONE_SECOND;
const ONE_HOUR = 60*ONE_MINUTE;
const ONE_DAY = 24*ONE_HOUR;
const BUCKET_NAME = 'utpower';
AWS.config.loadFromPath('./config.json');
const s3 = new AWS.S3();
let tags;
let page_size = 10;
const tag_max = 1000;
const uploadFile = (filename) => {
    const fileContent = fs.readFileSync(filename);
    const params = {
        Bucket: BUCKET_NAME,
        Key: "telemetry_historian_dynamic/"+filename,
        Body: fileContent
    };
    console.log("uploading ...");
    s3.upload(params, function(err, data) {
        if (err) {
            throw err
        }
        console.log(`File uploaded successfully. ${data.Location}`);
        const path = './';
        let regex = /[.]csv$/;
        console.log("unlinking ...");
        fs.readdirSync(path)
            .filter(f => regex.test(f))
            .map(f => fs.unlinkSync(f));
    });
};
let send_to_aws = async(tag_pointer) => {
    page_of_tags = tags.slice(tag_pointer, tag_pointer+page_size);
    const current_time = (new Date()).getTime();
    const from = current_time - ONE_DAY; 
    const to   = current_time
    const url = "https://10.101.206.21/webapi/api/trend/multitag?tags="+page_of_tags+"&start="+from+"&end="+to+"&interval=3600000";
    console.log(url);
	options = {
        url: url,
	    rejectUnauthorized: false
	};
	result = await request(options);
    let tag_series_map = JSON.parse(result).data;
    const day_file = [];
	for(const [tag, series] of Object.entries(tag_series_map)){
        const datum = series[0];
	    const one_record = {};
	    one_record.time = datum.T;
	    one_record.building = tag.substring(0,3);
	    one_record.utility = tag.substring(4,5);
	    one_record.value = datum.V;
	    one_record.quality = datum.Q;
        day_file.push(one_record);
	};
    let csv;
    try {
        csv = parser.parse(day_file);
        console.log(csv);
    } catch (err) {
        console.error(err);
    }
    const filename = ""+(new Date()).getTime()+".csv";
	try {
		await fs.promises.writeFile(filename, csv, "utf8");
	} catch (err) {
		console.log(err);
	}
	try {
		await uploadFile(filename);
	} catch (err) {
		console.log(err);
	}
}
let go = async() => {
	console.log("executing ...");
	let options = {
	    url: "https://10.101.206.21/webapi/api/taginfo/searchtag?condition=tbu_pd",
	    rejectUnauthorized: false
	};	
	console.log("requesting ...");
	let result = await request(options);
    let result_parsed = JSON.parse(result).data;
    tags = result_parsed.map(element => {return element.Tag});
    // tags = tags.slice(0,tag_max); // debug and testing
    console.info("tags.length: "+tags.length);
    const pages = Math.floor(tags.length / page_size);
    let tag_pointer = 0;
    console.info("tag_pointer: "+tag_pointer);
    await send_to_aws(tag_pointer);
    for(page = 1; page < pages; page++){
    	tag_pointer = tag_pointer + page_size;
    	console.info("tag_pointer: "+tag_pointer);
    	await send_to_aws(tag_pointer);
    }
}
go();
